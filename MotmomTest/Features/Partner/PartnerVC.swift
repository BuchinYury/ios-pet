//
//  PartnerViewController.swift
//  MotmomTest
//
//  Created by Yura Buchin on 10/03/2019.
//  Copyright © 2019 Yura Buchin. All rights reserved.
//

import UIKit

class PartnerViewController: UIViewController {
    
    private var partner: Partner?
    
    private var scrollView = createScrolView()
    private let partnerImg = createPartnerImg()
    private let partnerLongDescription = createPartnerLongDescription()
    
    convenience init(partner: Partner) {
        self.init()
        self.title = partner.name
        self.partner = partner
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        setAutolayout()
        
        PartnerViewController.bindPartnerImageView(partnerImg, partner?.imageHref)
        PartnerViewController.bindPartnerLongDescription(partnerLongDescription, partner?.longDescription)
    }
}

extension PartnerViewController {
    private static func createScrolView() -> UIScrollView {
        return UIScrollView()
    }
    
    private static func createPartnerLongDescription() -> UILabel {
        let view = UILabel()
        view.numberOfLines = 0
        return view
    }
    
    private static func createPartnerImg() -> UIImageView {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }
}

extension PartnerViewController {
    private static func bindPartnerImageView(_ partnerImg: UIImageView,
                                             _ imageHref: String?) {
        guard let imageHref = imageHref else { return }
        if var urlComponent = URLComponents(string: imageHref){
            urlComponent.scheme = "https"
            if let httpsPartnerImageHref = urlComponent.string {
                partnerImg.downloaded(from: httpsPartnerImageHref)
            }
        }
    }
    
    private static func bindPartnerLongDescription(_ partnerLongDescription: UILabel,
                                                   _ longDescription: String?){
        partnerLongDescription.text = longDescription
    }
}

extension PartnerViewController: Autolayouted {
    var viewHierarchy: ViewHierarchy {
        return .view(
            view,
            subhierarchy: [.view(scrollView,
                                 subhierarchy: [.view(partnerImg, subhierarchy: nil),
                                                .view(partnerLongDescription, subhierarchy: nil),]),]
        )
    }
    
    var autolayout: [Autolayouted.Autolayout] {
        return [
            (view: scrollView, layout: {
                $0.size.equalToSuperview()
            }),
            (view: partnerImg, layout: {
                $0.top.equalToSuperview().offset(16)
                $0.left.equalToSuperview().offset(16)
                $0.right.equalToSuperview().offset(-16)
            }),
            (view: partnerLongDescription, layout: {
                $0.centerX.equalToSuperview()
                $0.top.equalTo(self.partnerImg.snp.bottom).offset(16)
                $0.bottom.equalToSuperview().offset(-16)
                $0.left.equalToSuperview().offset(8)
                $0.right.equalToSuperview().offset(-8)
            }),
        ]
    }
}
