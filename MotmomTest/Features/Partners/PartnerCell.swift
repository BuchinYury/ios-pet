//
//  File.swift
//  MotmomTest
//
//  Created by Yura Buchin on 17/04/2019.
//  Copyright © 2019 Yura Buchin. All rights reserved.
//

import RxSwift

class PartnerCell: UITableViewCell {
    static let cellIdentifire = "PartnerCell"
    
    var partnerModel: Partner? {
        didSet {
            guard let partnerModel = partnerModel else { return }
            partnerName.text = partnerModel.name
            partnerImg.image = nil
            if let partnerImageHref = partnerModel.imageHref {
                if var urlComponent = URLComponents(string: partnerImageHref){
                    urlComponent.scheme = "https"
                    if let httpsPartnerImageHref = urlComponent.string {
                        partnerImg.downloaded(from: httpsPartnerImageHref)
                    }
                }
            }
        }
    }
    
    private let partnerImg = createPartnerImg()
    private let partnerName = createPartnerLable()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .clear
        self.selectionStyle = .none
        setAutolayout()
    }
    
    func set(partnerModel: Partner) {
        self.partnerModel = partnerModel
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension PartnerCell {
    private static func createPartnerImg() -> UIImageView {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }
    
    private static func createPartnerLable() -> UILabel {
        return UILabel()
    }
}

extension PartnerCell: Autolayouted {
    var viewHierarchy: ViewHierarchy {
        return .view(
            contentView,
            subhierarchy: [
                .view(partnerName, subhierarchy: nil),
                .view(partnerImg, subhierarchy: nil),
            ]
        )
    }
    
    var autolayout: [Autolayouted.Autolayout] {
        return [
            (view: partnerImg, layout: {
                $0.left.equalToSuperview().offset(16)
                $0.size.equalTo(100)
                $0.top.equalToSuperview()
                $0.bottom.equalToSuperview()
            }),
            (view: partnerName, layout: {
                $0.centerY.equalToSuperview()
                $0.left.equalTo(self.partnerImg.snp.right).offset(16)
                $0.right.equalToSuperview()
            }),
        ]
    }
}
