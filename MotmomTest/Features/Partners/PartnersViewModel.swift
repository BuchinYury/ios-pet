//
//  PartnersViewModel.swift
//  MotmomTest
//
//  Created by Yura Buchin on 09/03/2019.
//  Copyright © 2019 Yura Buchin. All rights reserved.
//

import Foundation
import RxSwift
import Moya

struct PartnersViewState {
    let partners: [Partner]
    let isLoading: Bool
    let error: Error?
    var isPartnerHidden: Bool  {
        get {
            return error != nil
        }
    }
    var isErrorHidden: Bool  {
        get {
            return error == nil
        }
    }
    
    init(partners: [Partner] = [], isLoading: Bool = false, error: Error? = nil) {
        self.partners = partners
        self.isLoading = isLoading
        self.error = error
    }
    
    func copy(partners: [Partner]? = nil, isLoading: Bool? = nil, error: Error? = nil) -> PartnersViewState {
        return PartnersViewState(partners: partners ?? self.partners,
                                 isLoading: isLoading ?? self.isLoading,
                                 error: error)
    }
}

enum PartnersIntention {
    case viewDidLoad, retryLoadPartnersButtonPressed, refreshPartnersInit
}

enum PartnersSideEffect {
}

class PartnersViewModel {
    typealias StateReducer = (PartnersViewState) -> PartnersViewState

    var viewState: Observable<PartnersViewState> = PublishSubject()

    let intentions = PublishSubject<PartnersIntention>()
    private let disposeBag = DisposeBag()

    init(initialState: PartnersViewState = PartnersViewState()) {
        let loadPartners = intentions
            .filter { intention in
                switch intention {
                    case .viewDidLoad, .retryLoadPartnersButtonPressed, .refreshPartnersInit: return true
                }
            }
            .flatMap { _ in
                backend.rx.request(.loadPartners)
                    .asObservable()
                    .map([String: Partner].self)
                    .map { (loadPartnersData) -> [Partner] in
                        Array(loadPartnersData.values)
                    }
                    .map { (partners: [Partner]) -> StateReducer in
                            { (currentState: PartnersViewState) -> PartnersViewState in
                                return currentState.copy(partners: partners, isLoading: false, error: nil)
                            }
                    }
                    .startWith({ (currentState: PartnersViewState) -> PartnersViewState in
                        currentState.copy(isLoading: true, error: currentState.error)
                    })
                    .catchError { (error) in
                        print("error \(error)")
                        return Observable.just { (currentState: PartnersViewState) in
                            currentState.copy(isLoading: false, error: error)
                        }
                }
        }

        Observable.merge(loadPartners)
            .scan (initialState, accumulator: {(currentState: PartnersViewState, stateReducer: StateReducer) -> PartnersViewState in
                return stateReducer(currentState)
            })
            .subscribe(onNext: { (newState: PartnersViewState) -> Void in
                (self.viewState as? PublishSubject)?.onNext(newState)
            })
            .disposed(by: disposeBag)
    }
}
