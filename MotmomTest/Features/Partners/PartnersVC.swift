//
//  ViewController.swift
//  MotmomTest
//
//  Created by Yura Buchin on 05/03/2019.
//  Copyright © 2019 Yura Buchin. All rights reserved.
//

import UIKit
import Moya
import RxSwift
import RxCocoa

class PartnersViewController: UIViewController {
    private let viewModel = PartnersViewModel()
    private let disposeBag = DisposeBag()
   
    private let errorView = createErrorView()
    private let partnersTableView = createPartnersTableView()
    let refreshControl = createrefreshControl()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Партнеры"
        
        setAutolayout()
        
        PartnersViewController.configureErrorView(
            errorView,
            viewModel,
            disposeBag
        )
        PartnersViewController.configureRefreshControl(
            refreshControl,
            viewModel,
            disposeBag
        )
        PartnersViewController.configurePartnersTableView(
            partnersTableView,
            navigationController,
            viewModel,
            disposeBag
        )

        viewModel.intentions.onNext(.viewDidLoad)
    }
}

extension PartnersViewController: Autolayouted {
    var viewHierarchy: ViewHierarchy {
        return .view(
            view,
            subhierarchy: [
                .view(errorView,
                      subhierarchy: nil),
                .view(partnersTableView,
                      subhierarchy: [.view(refreshControl, subhierarchy: nil)]),
            ]
        )
    }
    
    var autolayout: [Autolayouted.Autolayout] {
        return [
            (view: errorView, layout: {
                $0.center.equalToSuperview()
                $0.size.equalToSuperview()
            }),
            (view: partnersTableView, layout: {
                $0.center.equalToSuperview()
                $0.size.equalToSuperview()
            }),
        ]
    }
}

extension PartnersViewController {
    private static func createErrorView() -> ErrorView {
        return ErrorView()
    }

    private static func createPartnersTableView() -> UITableView {
        let view = UITableView()
        view.backgroundColor = .clear
        view.estimatedRowHeight = 100
        view.rowHeight = UITableView.automaticDimension
        view.register(PartnerCell.self, forCellReuseIdentifier: PartnerCell.cellIdentifire)
        return view
    }
    
    private static func createrefreshControl() -> UIRefreshControl {
        return UIRefreshControl()
    }
}

extension PartnersViewController {
    static private func configurePartnersTableView(_ tableView: UITableView,
                                                   _ navigationController: UINavigationController?,
                                                   _ viewModel: PartnersViewModel,
                                                   _ disposeBag: DisposeBag) {
        viewModel.viewState
            .map { currentState in
                currentState.partners
            }
            .distinctUntilChanged()
            .bind(to: tableView.rx.items(
                cellIdentifier: PartnerCell.cellIdentifire,
                cellType: PartnerCell.self)) { (row, element, cell) in
                cell.set(partnerModel: element)
            }
            .disposed(by: disposeBag)
        
        viewModel.viewState
            .map { currentState in
                currentState.isPartnerHidden
            }
            .distinctUntilChanged()
            .subscribe { event in
                if let isPartnerHidden = event.element {
                    tableView.isHidden = isPartnerHidden
                }
            }
            .disposed(by: disposeBag)
        
        tableView.rx.itemSelected
            .bind {
                let cell = tableView.cellForRow(at: $0) as? PartnerCell
                guard let partner = cell?.partnerModel else { return }
                
                let partnerVC = PartnerViewController(partner: partner)
                navigationController?.pushViewController(partnerVC, animated: true)
            }
            .disposed(by: disposeBag)
    }
    
    static private func configureErrorView(_ errorView: ErrorView,
                                           _ viewModel: PartnersViewModel,
                                           _ disposeBag: DisposeBag) {
        errorView.retryButton
            .rx.tap
            .subscribe {_ in
                viewModel.intentions.onNext(.retryLoadPartnersButtonPressed)
            }
            .disposed(by: disposeBag)
        
        viewModel.viewState
            .map { currentState in
               currentState.isErrorHidden
            }
            .distinctUntilChanged()
            .subscribe { event in
                if let isErrorHidden = event.element {
                    errorView.isHidden = isErrorHidden
                }
            }
            .disposed(by: disposeBag)
    }
    
    static private func configureRefreshControl(_ refreshControl: UIRefreshControl,
                                                _ viewModel: PartnersViewModel,
                                                _ disposeBag: DisposeBag) {
        viewModel.viewState
            .distinctUntilChanged { currentState, newState in
                currentState.isLoading == newState.isLoading
            }
            .map { currentState in
                currentState.isLoading
            }
            .subscribe { event in
                if let isLoading = event.element {
                    if isLoading {
                        refreshControl.beginRefreshing()
                    } else {
                        refreshControl.endRefreshing()
                    }
                }
            }
            .disposed(by: disposeBag)
        
        refreshControl.rx.controlEvent(.valueChanged)
            .subscribe { event in
                viewModel.intentions.onNext(.refreshPartnersInit)
            }
            .disposed(by: disposeBag)
    }
}
