//
//  Model.swift
//  MotmomTest
//
//  Created by Yura Buchin on 05/03/2019.
//  Copyright © 2019 Yura Buchin. All rights reserved.
//

struct Partner: Decodable, Equatable{
    let id: String?
    let name: String?
    let imageHref: String?
    let shortDescription: String?
    let longDescription: String?
}
