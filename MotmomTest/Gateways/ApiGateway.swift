//
//  ApiGateway.swift
//  MotmomTest
//
//  Created by Yura Buchin on 08/03/2019.
//  Copyright © 2019 Yura Buchin. All rights reserved.
//

import Foundation
import Moya

let backend = MoyaProvider<BackendApi>()

enum BackendApi {
    case loadPartners
}

extension BackendApi: TargetType {
    var baseURL: URL { return URL(string: "https://project-8716260381830912889.firebaseio.com")! }
    var path: String {
        switch self {
        case .loadPartners:
            return "/offices.json"
        }
    }
    var method: Moya.Method {
        switch self {
        case .loadPartners:
            return .get
        }
    }
    var task: Task {
        switch self {
        case .loadPartners:
            return .requestPlain
        }
    
    }
    var sampleData: Data {
        switch self {
        case .loadPartners:
            return Data()
        }
    }
    var headers: [String: String]? {
        return nil
    }
}

