//
//  Extentions.swift
//  MotmomTest
//
//  Created by Yura Buchin on 09/03/2019.
//  Copyright © 2019 Yura Buchin. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

enum ViewHierarchy {
    case view(UIView, subhierarchy: [ViewHierarchy]?)
   
    private func build(in superview: UIView) {
        switch self {
        case .view(let view, subhierarchy: let subhierarchy):
            superview.addSubview(view)
            subhierarchy?.forEach { $0.build(in: view) }
        }
    }
    
    func build() {
        switch self {
        case .view(let view, subhierarchy: let subhierarchy):
            subhierarchy?.forEach { $0.build(in: view) }
        }
    }
}


protocol Autolayouted {
    typealias Autolayout = (view: UIView, layout: (ConstraintMaker) -> Void)
    var viewHierarchy: ViewHierarchy { get }
    var autolayout: [Autolayout] { get }
}

extension Autolayouted {
    func setAutolayout() {
        viewHierarchy.build()
        autolayout.forEach { $0.view.snp.makeConstraints($0.layout) }
    }
}
