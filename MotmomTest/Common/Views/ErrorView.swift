//
//  ErrorView.swift
//  MotmomTest
//
//  Created by Yura Buchin on 15/04/2019.
//  Copyright © 2019 Yura Buchin. All rights reserved.
//

import UIKit

class ErrorView: UIView {
    let errorLabel = createErrorLabel()
    let retryButton = createRetryButton()
    
    init(){
        super.init(frame: .zero)
        setAutolayout()
        self.backgroundColor = .white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ErrorView {
    private static let errorLableDefaultText = "При загрузке данных произошла ошибка."
   
    private static func createErrorLabel() -> UILabel {
        let view = UILabel()
        view.text = errorLableDefaultText
        view.numberOfLines = 0
        view.textAlignment = .center
        return view
    }
    
    private static func createRetryButton() -> UIButton {
        let view = UIButton(type: .roundedRect)
        view.setTitle("Повторить", for: .normal)
        return view
    }
}

extension ErrorView: Autolayouted {
    var viewHierarchy: ViewHierarchy {
        return .view(
            self,
            subhierarchy: [
                .view(errorLabel, subhierarchy: nil),
                .view(retryButton, subhierarchy: nil),
            ]
        )
    }
    
    var autolayout: [Autolayouted.Autolayout] {
        return [
            (view: errorLabel, layout: {
                $0.bottom.equalTo(self.retryButton.snp.top).offset(-16)
                $0.centerX.equalToSuperview()
                $0.left.equalToSuperview()
                $0.right.equalToSuperview()
            }),
            (view: retryButton, layout: {
                $0.center.equalToSuperview()
            }),
        ]
    }
}
